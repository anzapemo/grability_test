/**
 * 10/05/2017
 * anzapemo
 * Script principal
 * Con el uso de browserify se buscan los otros módulos
 * Aquí se configura Angular en su totalidad
 */
 var angular = require("angular");
 var uiRoute = require("angular-ui-router/release/angular-ui-router");
 window.$ = window.jQuery = require('jquery');
 var bootstrap = require("bootstrap");
 
 angular
 	//App
 	.module("marvel_app",[uiRoute.default])
 	//Routing
 	.config(require("./angular/routes/main.js"))
 	//Directivas
 	.directive("searchbar", require("./angular/directives/searchBarDirective.js"))
 	.directive("sortbycontrol", require("./angular/directives/sortByDirective.js"))
 	.directive("charactercover", require("./angular/directives/characterCoverDirective.js"))
 	.directive("characterdetail", require("./angular/directives/characterDetailDirective.js"))
 	.directive("comicdetail", require("./angular/directives/comicDetailDirective.js"))
 	.directive("favouritecomic", require("./angular/directives/favouriteComicDirective.js"))
 	//Servicios
 	.factory("marvelapiservice", require("./angular/services/marvelApiService.js"))
 	.factory("marvelcharactersservice", require("./angular/services/marvelCharactersService.js"))
 	.factory("marvelcomicsservice", require("./angular/services/marvelComicsService.js"))
 	.factory("localstorageservice", require("./angular/services/localStorageService.js"))
 	.factory("favouritesservice", require("./angular/services/favouritesService.js"))
 	//Controladores
 	.controller("appcontroller", require("./angular/controllers/appController.js"));
