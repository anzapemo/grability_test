"use strict";

/**
 * Routing y direccionamiento de la aplicación.
 * Como se trata de una SPA de vistas integradas, no se usa más que la ruta /index
 */
module.exports = function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise("/index");
	$stateProvider
        .state("index", {
            url: "/index",
            templateUrl: "js/angular/partials/index.html",
            controller: "appcontroller"
        });
}