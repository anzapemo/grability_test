/**
 * anzapemo
 * 10/05/2017
 * Controlador principal de la aplicación
 */

"use strict";


module.exports = ["$scope", "marvelcharactersservice", "marvelcomicsservice", "favouritesservice", function($scope, marvelcharactersservice, marvelcomicsservice, favouritesservice){
	
	//Variable que tendrá lo que busque el usuario en la barra de búsqeuda
	$scope.searchbarText = "";

	//Variable con el mensaje de error de búsqueda
	$scope.searchbarErrorMsg = "";

	//Variable de control que indicará si se está haciendo una búsqueda
	$scope.searching = false;

	//Variable que controla el número de characters a mostrar por página
	$scope.maxResultsPerPage = 10;

	//Variable que indica el número de páginas a mostrar
	$scope.numPages = [];

	//Arreglo de los resultados mostrados en la página
	$scope.pageResults = [];

	//Arreglo con los resultados generales recibidos
	$scope.generalResults = [];

	//Variable para saber qué página está seleccionada
	$scope.currentPage = 1;

	//Filtros de ordenamiento
	$scope.orderByFilters = [
		{
			name: "Name",
			value: marvelcharactersservice.marvelApiOrderByName
		},
		{
			name: "Name desc",
			value: marvelcharactersservice.marvelApiOrderByNameDesc
		},
		{
			name: "Modified",
			value: marvelcharactersservice.marvelApiOrderByModified
		},
		{
			name: "Modified desc",
			value: marvelcharactersservice.marvelApiOrderByModifiedDesc
		}
	];

	//Filtro por defecto
	$scope.defaultFilter = $scope.orderByFilters[0];

	//Filtro seleccionado
	$scope.selectedFilter = -1;

	//Personaje actual seleccionado
	$scope.currentCharacter = null;	

	//Comic actual seleccionado
	$scope.currentComic = null;

	//Favoritos
	$scope.favourites = null;

	/**
	 * Funciones referentes a la búsqueda de "characters"
	 */	

	/**
	 * Se detecta si se ha presionado la tecla enter o el click a la lupa de
	 * la barra de búsqueda
	 */
	$scope.searchCharacter = function($event) {
		if ($scope.searching || $scope.searchbarText.length == 0){
			return;
		}

		if ($event !== undefined) {			
			if ($event.keyCode == 13) {
				$scope.searchByName($scope.selectedFilter, $scope.searchbarText);
			}
		} else {
			$scope.searchByName($scope.selectedFilter, $scope.searchbarText);
		}
	}

	/**
	 * Función que recibe los resultados y calcula la paginación 
	 */
	$scope.receiveResults = function(results){
		$scope.searchbarErrorMsg = "";
		$scope.numPages = [];
		$scope.pageResults = [];
		if (results && results.length > 0) {
			//Se calcula el número de páginas
			var numResults = results.length;
			var tail = (numResults % $scope.maxResultsPerPage) > 0 ? 1 : 0;			
			var numPages = parseInt(numResults / $scope.maxResultsPerPage) + tail;
			for (var i = 0; i < numPages; i++){
				$scope.numPages.push(i+1);
			}
			//Por defecto mostrarémos los primeros  $scope.maxResultsPerPage resultados			
			$scope.generalResults = results;
			$scope.pageResults = results.slice(0, $scope.maxResultsPerPage); //Array.prototype.slice no es inclusivo
			$scope.cleanPageResults();
			$scope.currentPage = 1;

		} else {
			$scope.searchbarErrorMsg = "There's nothing to show you ... try again please!";			
		}
	}

	/**
	 * Función que recibe los resultados y los limpia  
	 */
	$scope.cleanPageResults = function() {		
		$scope.pageResults.forEach(function(val, index){
			//Limitamos el número de comics a 4
			val.comics.items = val.comics.items.slice(0,4);
			//Si trae descripción la cortamos, de lo contrario mostramos la indicación
			if (val.description.length == 0) {
				val.description = "No description";
			} else {
				val.description = val.description.substring(0,200) + "...";	
			}			
		});
	}

	/**
	 * Busca a todos los personajes
	 */
	$scope.searchAllCharacters = function(filter){
		
		//Si no hay filtro seleccionado dejará el filtro por defecto
		if (filter == -1) {
			filter = $scope.defaultFilter.value;
		}

		$scope.searching = true;

		try {
			//Solicita al servicio buscar los personajes
			marvelcharactersservice.getAllCharacters(function(data){
				if (data != null){
					$scope.searchbarErrorMsg = "";
					//Recibe los resultados
					$scope.receiveResults(data);
				} else {
					$scope.searchbarErrorMsg = "There's nothing to show you ... try again please!";
				}
				$scope.searching = false;
			}, 100, filter);
		} catch (e) {
			$scope.searchbarErrorMsg = "¡Error! ... Please try again or reload the page!";			
		}
	}

	/**
	 * Busca los personajes cuyo nombre empiece por charactername
	 */	
	$scope.searchByName = function(filter, charactername){
		//Si no hay filtro seleccionado dejará el filtro por defecto
		if (filter == -1) {
			filter = $scope.defaultFilter.value;
		}

		$scope.searching = true;

		try {
			//Solicita al servicio buscar los personajes dada una parte del nombre
			marvelcharactersservice.getCharactersByName(function(data){
				if (data != null){
					$scope.searchbarErrorMsg = "";					
					//Recibe los resultados
					$scope.receiveResults(data);
				} else {
					$scope.searchbarErrorMsg = "There's nothing to show you ... try again please!";
				}
				$scope.searching = false;
			}, 100, filter, charactername);
		} catch (e) {
			$scope.searchbarErrorMsg = "¡Error! ... Please try again or reload the page!";			
		}
	}

	/**
	 * Busca todos los personajes o lo que se haya escrito en la
	 * barra de búsqueda con un filtro
	 */
	$scope.searchOrdered = function(filter){
		if ($scope.searchbarText.length > 0) {
			$scope.searchByName(filter, $scope.searchbarText);
		} else {
			$scope.searchAllCharacters(filter);
		}
	}

	/**
	 * Busca el detalle de un personaje seleccionado
	 * 
	 */
	$scope.showCharacterDetail = function(character) {
		$scope.currentCharacter = null;
		marvelcharactersservice.getCharacterById(function(data) {
			if (data != null) {
				$scope.currentCharacter = data;
				$scope.showCharacterDetailPopup();
			}
		}, character.id);
	}

	/**
	 * Muestra el popup del detalle de personajes	 
	 */
	$scope.showCharacterDetailPopup = function() {		
		$scope.centerDivOnScreen("characterdetailpopup");
		$("#characterdetailpopup").fadeIn();
	}

	/**
	 * Oculta el popup del detalle de personajes	 
	 */
	$scope.hideCharacterDetailPopup = function() {
		$("#characterdetailpopup").fadeOut();
	}

	/**
	 * Busca el detalle de un comic seleccionado
	 * 
	 */
	$scope.showComicDetail = function(comic) {
		$scope.currentComic = null;
		try {
			if (comic.resourceURI != null) {
				marvelcomicsservice.getComicByURL(function(data){
					if (data != null) {
						console.log(data);
						$scope.currentComic = data;
					}
				}, comic.resourceURI);
				$scope.showComicDetailPopup();	
			}
		} catch (e) {}
	}

	/**
	 * Muestra el popup del detalle de personajes	 
	 */
	$scope.showComicDetailPopup = function() {		
		$scope.centerDivOnScreen("comicdetailpopup");
		$("#comicdetailpopup").fadeIn();
	}

	/**
	 * Oculta el popup del detalle del comic seleccionado
	 */
	$scope.hideComicDetailPopup = function() {
		$("#comicdetailpopup").fadeOut();
	}

	/**
	 * Centra un div en la mitad de la pantalla
	 */
	$scope.centerDivOnScreen = function(divId) {
		$(("#" + divId)).css({
			left: ($(window).width() - $(("#" + divId)).outerWidth())/2,
        	top: ($(window).height() - $(("#" + divId)).outerHeight())/2
		});
	}

	/**
	 *
	 * SECCIÓN PARA CONTROLAR LA PAGINACIÓN
	 *
	 */

	 /**
	  * Cambia al número de página recibido por parámetro
	  * Para ello se "corta" el array de resultados general sabiendo el $scope.maxResultsPerPage
	  */
	 $scope.changePageNumber = function(pageNumber) {
	 		$scope.currentPage = pageNumber;
	 		var max = pageNumber * $scope.maxResultsPerPage;
	 		var min = max - $scope.maxResultsPerPage;
	 		$scope.pageResults = $scope.generalResults.slice(min, max);
	 		$scope.cleanPageResults();
	 }

	 /**
	  * Función para cambiar de página
	  * Si el parámetro es back o forward se adelanta o atrasa una página, de lo contrario se busca el número de página
	  */
	 $scope.changePage = function(param){		 	
	 	if (param == 'back') {
	 		if ($scope.currentPage != $scope.numPages[0]) {
	 			$scope.changePageNumber($scope.currentPage - 1);
	 		}
	 	} else if (param == 'forward') {
	 		if ($scope.currentPage != $scope.numPages[($scope.numPages.length - 1)]) {
	 			$scope.changePageNumber($scope.currentPage + 1);	
	 		}
	 	} else {
	 		try {
	 			$scope.changePageNumber(parseInt(param));
	 		} catch (e) {}	
	 	}	 	
	 }

	 /**
	  * Carga los favoritos
	  */
	 $scope.loadFavorites = function() {
	 	$scope.favourites = favouritesservice.getFavourites();
	 }

	 /**
	  * Guarda un comic en los favoritos
	  */
	 $scope.addToFavourites = function(comic) {	 	
	 	favouritesservice.saveFavComic(comic);
	 	$scope.loadFavorites();
	 }

	 /**
	  * Guarda un comic en los favoritos
	  */
	 $scope.removeFromFavourites = function(comic) {	 	
	 	favouritesservice.deleteFavComic(comic);
	 	$scope.loadFavorites();
	 }	

	/**
	 * Función para seleccionar 3 comics al azar de la lista de personajes en pantalla
	 * Se intentará 100 veces buscar 3 comics, ya que o bien puede que los personajes no tengan comics
	 * o que los comics no tengan un resourceURI
	 */
	$scope.selectRandomComics = function() {
		//Se toman los resultados mostrados en pantalla y se recorren al azar
		var globalTries = 0;
		var maxGlobalTries = 100;
		for (var j = 0; j < 3; j++) {
			globalTries++;
			//Seleccionamos 3 personajes al azar, y luego seleccionamos 1 comic al azar de dicho personaje			
			var randomCharacterIndex = Math.floor((Math.random() * $scope.pageResults.length) + 1);
			var randomCharacter = $scope.pageResults[randomCharacterIndex - 1];
			//Si el personaje no tiene comics se omite y se continua
			if (randomCharacter.comics.items.length == 0) {
				if (globalTries < maxGlobalTries) {
					j = j -1;	
				}				
				continue;
			}
			var randomComicIndex = Math.floor((Math.random() * randomCharacter.comics.items.length) + 1);
			try {
				marvelcomicsservice.getComicByURL(function(data){
					if (data != null) {
						$scope.addToFavourites(data);
					}
				}, randomCharacter.comics.items[randomComicIndex - 1].resourceURI);
			} catch (e) {
				if (globalTries < maxGlobalTries) {
					j = j -1;	
				}				
				continue;
			}
		}		 
	}

	/**
	 * Función con la que arrancará el controller
	 */
	$scope.init = function() {
		$scope.searchAllCharacters($scope.defaultFilter.value);		
		$scope.loadFavorites();
	}

	$scope.init();

}];