"use strict"

/** 
 * Directiva para mostrar un favorito
 **/
module.exports = function() {
	return {
		scope : {
			fav : "="
		},
		replace: true,
		restrict : 'EA',
		templateUrl : "js/angular/templates/favouriteComicTemplate.html"
	}
}