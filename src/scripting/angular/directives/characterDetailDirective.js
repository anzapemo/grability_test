"use strict"

/**
 * Directiva para el detalle de un character
 */
module.exports = function() {
	return {
		scope : {
			character : "="
		},
		replace: true,
		restrict : 'EA',
		templateUrl : "js/angular/templates/characterDetailTemplate.html"
	}
}