"use strict"

/** 
 * Directiva para una barra de búsqueda
 */ 
module.exports = function() {
	return {
		replace: true,
		restrict : 'EA',
		templateUrl : "js/angular/templates/searchbarTemplate.html"
	}
}