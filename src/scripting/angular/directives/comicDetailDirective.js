"use strict"

/**
 * Directiva para mostrar el detalle de un comic
 *
 */
module.exports = function() {
	return {
		scope : {
			comic : "="
		},
		replace: true,
		restrict : 'EA',
		templateUrl : "js/angular/templates/comicDetailTemplate.html"
	}
}