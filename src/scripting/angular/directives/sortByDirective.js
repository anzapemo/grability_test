"use strict"

/**
 * Directiva para el control de ordenamiento
 *
 */ 
module.exports = function() {
	return {
		scope: {
			characterfilters : "="
		},
		replace: true,
		restrict : 'EA',
		templateUrl : "js/angular/templates/sortByTemplate.html"
	}
}