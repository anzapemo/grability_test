"use strict"
/**
 * Directiva para el preview de un character
 */
module.exports = function() {
	return {
		scope : {
			character : "="
		},
		replace: true,
		restrict : 'EA',
		templateUrl : "js/angular/templates/characterCoverTemplate.html"
	}
}