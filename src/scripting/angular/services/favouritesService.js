/**
 * anzapemo
 * 13/05/2017
 * Servicio para guardar y recuperar favoritos 
 */

"use strict"

module.exports = ["localstorageservice", function(localstorageservice){
	
	var fvService = {};

	fvService.favouritesArrayName = "favs";

	/**
	 * Recupera la lista de favoritos
	 * retorna null en caso de error, el array de favoritos en caso contrario
	 */
	fvService.getFavourites = function() {
		var favs = localstorageservice.getObject(fvService.favouritesArrayName);
		if (favs == null) {
			favs = [];
		}
		return favs;
	}

	/**
	 * Guarda los favoritos en el localStorage
	 * 
	 */
	fvService.saveFavorites = function(favs) {
		return localstorageservice.saveObject(fvService.favouritesArrayName,favs);
	}

	/**
	 * Comprueba si un comic está guardado en los favoritos.
	 * retorna el indice en el array de favoritos si se encuentra el comic o false según el caso
	 */
	fvService.isComicAdded = function(comic) {
		var favs = fvService.getFavourites();
		if (favs != null) {
			for (var i = 0; i < favs.length; i++){
				if (favs[i].id == comic.id) {
					return i;
				}
			}
			return false;
		} else {
			return false;
		}
	}	

	/**
	 * Guarda un comic en los favoritos, comprueba si ya está guardado antes de
	 * Retorna 0 si se guardó el comic, null de lo contrario
	 */
	fvService.saveFavComic = function(comic) {
		if (fvService.isComicAdded(comic) === false) {
			var favs = fvService.getFavourites();
			if (favs != null) {
				comic.custominfo = {
						added : true
					};
				favs.push(comic);
				return fvService.saveFavorites(favs);			
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * Borra un comic de favoritos
	 * Retorna 0 si se guardó, null de lo contrario
	 */
	fvService.deleteFavComic = function(comic) {
		var index = fvService.isComicAdded(comic);
		if (index !== false) {
			var favs = fvService.getFavourites();
			if (favs != null) {
				favs.splice(index, 1);
				return fvService.saveFavorites(favs);			
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	return fvService;

}];