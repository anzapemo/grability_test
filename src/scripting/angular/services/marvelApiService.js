/**
 * anzapemo
 * 10/05/2017
 * Servicio para recuperar los datos del servidor de Marvel
 */

"use strict"

module.exports = ["$http", function($http){
	
	//Declaramos lo que la factoría inyectará como servicio
	var maService = {};

	//Variables para comunicarse contra el API de Marvel
	maService.marvelApiUrl = "https://gateway.marvel.com:443";	
	maService.marvelApiKey = "dc36596e383152172bb69b2a75192404";	
	maService.marvelHash = "42759a4b830512926adbf1282c6eaad5";
	maService.marvelApiKeyUrlPart = "apikey=" + maService.marvelApiKey + "&ts=1&hash=" + maService.marvelHash;

	//Url para recuperar characters de marvel
	maService.marvelApiCharactersUrl = maService.marvelApiUrl + "/v1/public/characters";	

	/**
	 * Función que permitirá recuperar todos los characters del API
	 * invocando luego una función callback
	 * @callback la función de llamado cuando se termine la solicitud
	 * @limit el límite de personajes que se desea traer
	 * @sortBy el ordenamiento
	 */
	maService.getAllCharacters = function(callback, limit, sortBy){		
		//Construimos la url
		var finalUrl = maService.marvelApiCharactersUrl + "?limit=" + limit + "&" + sortBy + "&" + maService.marvelApiKeyUrlPart;
		
		var request = {
			method: "GET",
			url: finalUrl
		};

		maService.httpRequest(callback, request);				
	}

	/**
	 * Función que permitirá recuperar todos los characters del API dado un indicio de nombre
	 * invocando luego una función callback
	 * @callback la función de llamado cuando se termine la solicitud
	 * @limit el límite de personajes que se desea traer
	 * @sortBy el ordenamiento
	 * @name el String con el que empieza el nombre del personaje
	 */
	maService.getCharactersByName = function(callback, limit, sortBy, name){		
		//Construimos la url
		var finalUrl = maService.marvelApiCharactersUrl + "?nameStartsWith=" + name 
								+ "&limit=" + limit 
								+ "&" + sortBy 
								+ "&" + maService.marvelApiKeyUrlPart;
		
		var request = {
			method: "GET",
			url: finalUrl
		};

		maService.httpRequest(callback, request);				
	}

	/**
	 * Función que permitirá recuperar un character dado su id
	 * invocando luego una función callback
	 * @callback la función de llamado cuando se termine la solicitud	 
	 * @id el id (de Marvel) del character
	 */
	maService.getCharacterById = function(callback, id){		
		//Construimos la url
		var finalUrl = maService.marvelApiCharactersUrl + "/" + id + "?" + maService.marvelApiKeyUrlPart;
		
		var request = {
			method: "GET",
			url: finalUrl
		};

		maService.httpRequest(callback, request);				
	}

	/**
	 * Función que permitirá recuperar comic dada su URL
	 * invocando luego una función callback
	 * @callback la función de llamado cuando se termine la solicitud	 
	 * @url el resource URL que proporciona Marvel
	 */
	maService.getComicByURL = function(callback, url){		
		//Construimos la url
		var finalUrl = url + "?" + maService.marvelApiKeyUrlPart;
		
		var request = {
			method: "GET",
			url: finalUrl
		};

		maService.httpRequest(callback, request);				
	}

	/**
	 * Hace una petición http, en caso de error siempre llamará al callback con el parámetro false
	 */
	maService.httpRequest = function(callback, request){
		try {
			$http(request).then(
				function(data){
					try {
						if (data.status == 200) {
							callback(data.data);	
						} else {
							callback(false);	
						}
					} catch (e) {
						callback(false);
					}
				},
				function(){
					callback(false)	
				}
			);
		} catch (e) {
			callback(false);
		}
	}

	return maService;

}];