/**
 * anzapemo
 * 13/05/2017
 * Servicio para tratar los resultados del API de Marvel respecto a los comics
 */

"use strict"

module.exports = ["$http", "marvelapiservice", "favouritesservice", function($http, marvelapiservice, favouritesservice){
	
	//Declaramos lo que la factoría inyectará como servicio
	var mcService = {};

	/** 
	 *	Obtendrá la información de un comic dada su URL
	 *  Retornará un Array de JSON con los personajes, o null en caso de error (en la función callback)
	 */
	mcService.getComicByURL = function(callback, url){
		marvelapiservice.getComicByURL(function(data){
			//Se comprueba si hay data
			if (data) {							
				var results = data.data.results;				
				if (results != null){
					var comic = results[0];
					if (comic) {
						//Comprobamos que esté en los favoritos, de ser así se agrega el custom info
						if (favouritesservice.isComicAdded(comic) !== false) {
							comic.custominfo = {
								added : true
							};
						}
						callback(comic);
					} else {
						callback(null);
					}										
				} else {
					callback(null);
				}
				
			} else {				
				callback(null);
			}
		}, url);
	}
	
	return mcService;

}];