/**
 * anzapemo
 * 10/05/2017
 * Servicio para tratar los resultados del API de Marvel respecto a los personajes
 */

"use strict"

module.exports = ["$http", "marvelapiservice", function($http, marvelapiservice){
	
	//Declaramos lo que la factoría inyectará como servicio
	var mcService = {};

	mcService.marvelApiOrderBy = "orderBy=";
	mcService.marvelApiOrderByName = mcService.marvelApiOrderBy + "name";
	mcService.marvelApiOrderByNameDesc = mcService.marvelApiOrderBy + "-name";
	mcService.marvelApiOrderByModified = mcService.marvelApiOrderBy + "modified";
	mcService.marvelApiOrderByModifiedDesc = mcService.marvelApiOrderBy + "-modified";

	/** 
	 *	Obtendrá todos los characters que pueda del API
	 *  Retornará un Array de JSON con los personajes, o null en caso de error (en la función callback)
	 */
	mcService.getAllCharacters = function(callback, limit, orderBy){
		marvelapiservice.getAllCharacters(function(data){
			//Se comprueba si hay data
			if (data) {				
				var results = data.data.results;				
				callback(results);				
			} else {				
				callback(null)
			}
		}, limit, orderBy);
	}

	/** 
	 *	Obtendrá todos los characters que pueda del API (hasta @limit)
	 *  Retornará un Array de JSON con los personajes, o null en caso de error (en la función callback)
	 */
	mcService.getCharactersByName = function(callback, limit, orderBy, name){
		marvelapiservice.getCharactersByName(function(data){
			//Se comprueba si hay data
			if (data) {				
				var results = data.data.results;				
				callback(results);				
			} else {				
				callback(null)
			}
		}, limit, orderBy, name);
	}

	/** 
	 *	Obtendrá un character del api dado su id	 
	 */
	mcService.getCharacterById = function(callback, id) {
		marvelapiservice.getCharacterById(function(data){
			//Se comprueba si hay data
			if (data) {				
				var results = data.data.results;
				if (results.length > 0){
					callback(results[0]);					
				} else {
					callback(null);
				}
				
			} else {				
				callback(null);
			}
		}, id);
	}

	return mcService;

}];