/**
 * anzapemo
 * 13/05/2017
 * Servicio para guardar y recuperar valores en el localStorage
 * SIEMPRE SE DEBEN GUARDAR EN JSON
 */

"use strict"

module.exports = [function(){
	
	var lsService = {};

	/**
	 * Guarda un string en el localStorage
	 */
	lsService.saveString = function(key, string) {
		localStorage.setItem(key, string);
	}


	/**
	 * Recupera un string del localStorage
	 * 
	 */
	lsService.getString = function(key) {
		return localStorage.getItem(key);
	}

	/**
	 * Guarda un objeto en el local storage convirtiendo este a String
	 * retorna null en caso de error, 0 de lo contrario
	 */
	lsService.saveObject = function(key, object) {
		var stringObject = lsService.fromJsonToString(object);
		if (object != null) {
			lsService.saveString(key, stringObject);
			return 0;
		} else {
			return null;
		}
	}

	/**
	 * Obtiene un objeto del local storage 
	 * retorna null en caso de error, el objeto de lo contrario
	 */
	lsService.getObject = function(key) {
		var stringObject = lsService.getString(key);
		try {
			return lsService.fromStringToJson(stringObject);
		} catch (e) {
			return null;
		}
	}


 	/**
 	 * Convierte a JSON un String, devuelve null en caso de error 
 	 *
 	 */
	lsService.fromStringToJson = function(string) {
		try {
			return JSON.parse(string);
		} catch(e) {
			return null;
		}
	}

	/**
	 * Convierte un objeto Json a String
	 * retorna null en caso de error
	 */
	lsService.fromJsonToString = function(object){
		try {
			return JSON.stringify(object);
		} catch(e) {
			return null;
		}
	}

	return lsService;

}]; 