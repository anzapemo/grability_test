/**
 * anzapemo
 * 10/05/2017
 * Usamos gulp para procesar los archivos sass y moverlos a la carpeta dist
 **/

//Usamos gulp para convertir los archivos .scss a .css (sass pre-processor)
var gulp = require("gulp");
var sass = require("gulp-sass");
var browserify = require("gulp-browserify");

gulp.task("browserify", function(){
	gulp.src("src/scripting/app.js")
		.pipe(browserify())
		.pipe(gulp.dest("dist/js/"));
});



//Creamos una tarea que procesa los archivos sass
gulp.task("convert_sass", function(){
	gulp.src("src/styles/sass/*.scss")
		.pipe(sass({
			includePaths: ['./src/styles/sass/']
		}))
		.pipe(gulp.dest("dist/styles/"));
});

//Copia el index a dist
gulp.task("copy_index", function(){
	gulp.src("index.html")
			.pipe(gulp.dest("dist/"));
});

//Copia los estilos de boostrap a dist
gulp.task("copy_bootstrap", function(){
	gulp.src(["node_modules/bootstrap/dist/css/bootstrap.min.css","node_modules/bootstrap/dist/css/bootstrap-theme.min.css"])
			.pipe(gulp.dest("dist/styles/"));

	gulp.src("node_modules/bootstrap/dist/fonts/*")
			.pipe(gulp.dest("dist/fonts/"));
});

//Copia los templates de angular
gulp.task("copy_angular_templates", function(){
	gulp.src("src/scripting/angular/templates/*")
			.pipe(gulp.dest("dist/js/angular/templates/"));
});

//Copia los partials de angular
gulp.task("copy_angular_partials", function(){
	gulp.src("src/scripting/angular/partials/*")
			.pipe(gulp.dest("dist/js/angular/partials/"));
});

gulp.task("default", ["browserify", "convert_sass", "copy_index", "copy_bootstrap", "copy_angular_templates", "copy_angular_partials"]);

//Utlizamos un watcher para compilar los scss
gulp.task("sass", function(){
	gulp.watch("src/styles/sass/*.scss", ["convert_sass"]);
});
